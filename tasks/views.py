from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Task
from django.shortcuts import redirect
from django.urls import reverse_lazy


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    fields = ["name", "start_date", "due_date", "project", "assignee"]
    template_name = "projects/task_create.html"

    def form_valid(self, form):
        task = form.save(commit=False)
        task.members = self.request.user
        task.save()
        return redirect("show_project", pk=task.project.id)


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "projects/task_list.html"

    def get_queryset(self):
        return Task.objects.filter(assignee=self.request.user)


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
